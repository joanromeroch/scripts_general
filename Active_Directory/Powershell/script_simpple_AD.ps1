#Crear usuarios en AD y añadir al grupo 

Import-Module ActiveDirectory

$users = Import-Csv -Path "C:\Users\Administrador\Desktop\usuarios.csv" 
$password = ConvertTo-SecureString "1234Abcd" -AsPlainText -Force
$contador=0
foreach ($user in $users)
{
    $nombre = $user. 'nombre'
    $apellido = $user. 'apellido'
    $segundo_apellido = $user. 'segundo_apellido'
    $correo = $user. 'correo'
    $grupo = $user. 'grupo'
    $OUPATH = "OU=usuarios, OU=test, DC=test, DC=com"
    $usuario_principal= $nombre[0]+$apellido+$segundo_apellido[0]+$contador
    $comprobacion= Get-ADUser -Filter {EmailAddress -eq "$usuario_principal@test.com"}

    if ($comprobacion -eq $Null){
        New-AdUser -Name $usuario_principal -GivenName $nombre -Surname $apellido' '$segundo_apellido `
        -UserPrincipalName "$usuario_principal@test.com" -SamAccountName $usuario_principal -Path $OUPATH `
        -AccountPassword $password -Enabled $true -ChangePasswordAtLogon $true -EmailAddress $correo
        Add-ADGroupMember -Identity $grupo -Members "$usuario_principal" 
        Set-ADUser -Identity $usuario_principal
    $contador=$contador+1
    }
}

get-aduser -filter * -SearchBase "OU=test, DC=test,DC=com" -Properties Displayname, EmailAddress, Name, Surname | 
Select Name, Surname, Displayname, EmailAddress | Export-csv C:\listado_usuarios.csv -NoTypeInformation -Encoding UTF8
