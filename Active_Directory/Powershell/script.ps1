#Script para crear usuario AD, con perfiles moviles, unidad personal montada,añadir a grupos y crear buzon de exchange, al final hace una exportacion
Import-Module ActiveDirectory

$users = Import-Csv -Path "C:usuarios_csv.csv" 
$password = ConvertTo-SecureString "1234" -AsPlainText -Force
$contador=0
foreach ($user in $users)
{
    $nombre = $user. 'nombre'
    $apellido = $user. 'apellido'
    $segundo_apellido = $user. 'segundo_apellido'
    $correo = $user. 'correo'
    $grupo = $user. 'grupo'
    $OUPATH = 'Trbajadores'
    $usuario_principal= $nombre[0]+$apellido+$segundo_apellido[0]+$contador
    $personal = "\\WIN-PPBDRL4FUD6\carpetes_personals\$usuario_principal"
    $comprobacion= Get-ADUser -Filter {EmailAddress -eq "$usuario_principal@test.com"}
    $perfil= "\\WIN-PPBDRL4FUD6\" + $usuario_principal

    if ($comprobacion -eq $Null){
        New-Mailbox -Name $usuario_principal -FirstName $nombre -LastName $apellido' '$segundo_apellido  -OrganizationalUnit $OUPATH -Database "Mailbox Database" `
        -UserPrincipalName "$usuario_principal@test.com" -Password $password -ResetPasswordOnNextLogon $true -ErrorAction SilentlyContinue
        Add-ADGroupMember -Identity $grup -Members "$usuario_principal" 
        Set-ADUser -Identity $usuario_principal -HomeDrive H -HomeDirectory $personal -ProfilePath $perfil
        New-Item -Path $personal -Type directory -Force
    $contador=$contador+1
    }
}

get-aduser -filter * -SearchBase "OU=Trabajadores, DC=test,DC=com" -Properties Displayname, EmailAddress, Name, Surname | 
Select Name, Surname, Displayname, EmailAddress | Export-csv C:\listado_usuarios.csv -NoTypeInformation -Encoding UTF8
