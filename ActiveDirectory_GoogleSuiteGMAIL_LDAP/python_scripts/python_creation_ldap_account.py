#!/usr/bin/env python
import os
import ldap
import ldap.modlist as modlist

#---------------- Connect Ldap
con = ldap.initialize('ldap://ldap.test.com:389') ## modify with your ip or servername and port
user = "cn=Manager" ## admin account
password = "1234"
con.simple_bind_s(user, password)

# ---- I used this environ variables in jenkins, you can use normal variables
nombre = os.environ['nombre']
primer_apellido = os.environ['primer_apellido']
segundo_apellido = os.environ['segundo_apellido']
departamentos = os.environ['departamentos']
login1 = nombre + "." + primer_apellido+"@test.com"
correo = os.environ['correo']

if departamentos == "departamento_sistemas" or departamentos == "departamento_tecnico":  
    
    dn= "uid=" + nombre + "." + primer_apellido +"  ,OU=users, dc=test,dc=com"
    
    adduser = {
               "objectClass": ["inetOrgPerson"],
               "sn": [primer_apellido + " " + segundo_apellido],
               "uid": [nombre + "." + primer_apellido ],
               "mail": [correo],
               "givenName": [nombre],
               "cn": [nombre +" " + primer_apellido ],
               "displayName": [nombre + " " + primer_apellido + " " + segundo_apellido],
             }
    
    result = con.add_s(dn, ldap.modlist.addModlist(adduser))
    print("Usuario creado: ", dn)
else:
    print("No se creara el usuario en ldap para el departamento administracion, ni el departamento de comerciales")