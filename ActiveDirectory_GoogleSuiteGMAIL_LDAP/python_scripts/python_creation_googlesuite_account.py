#!/usr/bin/env python
from os import environ
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

#----- Variables
SERVICE_ACCOUNT_EMAIL = 'admin-sdk@admin-sdk-2445152356.iam.gserviceaccount.com'
SCOPES = ['https://www.googleapis.com/auth/admin.directory.user','https://www.googleapis.com/auth/admin.directory.group']
SERVICE_ACCOUNT_FILE='/usr/local/bin/scripts/key.p12'
#---- I used this environ variables in jenkins, you can use normal variables
nombre = environ['nombre']
primer_apellido = environ['primer_apellido']
departamentos = environ['departamentos']
login = nombre + "." + primer_apellido

#We need to put the credentiales so we can conect to the api, we also modifify admin account and add our key.p12 file we generated
#more info at their oficial documentation https://developers.google.com/admin-sdk/directory/v1/guides/authorizing
#----- Utilizamos las credenciales para conectar a la api de google y utilizamos una cuenta admininistrador
credentials = ServiceAccountCredentials.from_p12_keyfile(
   SERVICE_ACCOUNT_EMAIL,
   SERVICE_ACCOUNT_FILE,
   scopes=SCOPES )
credentials = credentials.create_delegated('administrador@test.com')

#----- User parameters account
#----- Parametros para usuario
userinfo = {'primaryEmail': login+'@test.com',
     'name': { 'givenName': nombre, 'familyName': primer_apellido },
    'password': '1234',
    'changePasswordAtNextLogin': 'true'
 }
#---- Creating users with service.users parameter
#---- Creamos el usuario pasandole el service.users
service = build("admin", "directory_v1", credentials=credentials)
service.users().insert(body=userinfo).execute()

#--- We add the users on the group with services.members
#--- Ponemos el usuario al grupo pasandole service.members
groupinfo = {'email': login+'@test.com'}

if departamentos == "departamento_sistemas":
  lista = ['testbcn@test.com','sistema@test.com','tecnico@test.com','comprasit@test.com','helpdesk@test.com']

elif departamentos == "departamento_comercial":
  lista = ['testbcn@test.com','comercial@test.com','hun@test.com','equcomercial@test.com']

elif departamentos == "departamento_tecnico":
  lista = ['equipotecnico@test.com','testbcn@test.com','tecnicos@test.com']

elif departamentos == "departamento_administracion":
  lista = ['testbcn@test.com','administracion@test.com','dominios@test.com', 'testtodos@test.com','equipoadministracion@test.com','rrhh@test.com']

for x in lista:
        service.members().insert(groupKey=x, body=groupinfo).execute()
