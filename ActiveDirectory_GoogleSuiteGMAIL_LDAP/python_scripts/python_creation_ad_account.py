#!/usr/bin/env python
from os import environ
import ldap
import ldap.modlist as modlist

#---------------- Connect Active Directory
con = ldap.initialize('ldap://192.168.1.55') ## YOUR ldap ip or name server
user = "ldap@test.com" ## admin account
password = "1234"  ## password
con.simple_bind_s(user, password)

#------- I used this environ variables in jenkins, you can use normal variables
nombre = environ['nombre']
primer_apellido = environ['primer_apellido']
segundo_apellido = environ['segundo_apellido']
departamentos = environ['departamentos']
login = nombre + "." + primer_apellido

#----------------------USER TO SPECIFIC GROUPS AND OU, ALWAYS PUT CORRECT PATH OU AND GROUP

if departamentos == "departamento_sistemas":
  dn = "CN=" + nombre + " " + primer_apellido + " ,OU=Usuarios, OU=normals,OU=test,dc=test,dc=local"
  group_dn = ["CN=normals, OU=Usuarios,OU=normals,OU=test,DC=test,DC=local","CN=Tecnicos,OU=Tecni,OU=Usuarios,OU=BCN,OU=test,DC=test,DC=local","CN=All,OU=test,DC=test,DC=local","CN=test,OU=test, DC=test,DC=local","CN=CVS-Users, OU=Tecnicos,OU=Usuarios,OU=BCn,OU=test,DC=test,DC=local","CN=Administ,OU=Dimp,OU=Usuarios,OU=BCN,OU=test,DC=test,DC=local"]

elif departamentos == "departamento_comercial":
  dn = "CN=" + nombre + " " + primer_apellido + " ,OU=Mid,OU=comp,OU=Usuarios,OU=BCN,OU=test,dc=test,dc=local"
  group_dn = ["CN=comp,OU=Comercial,OU=Usuarios,OU=BCN,OU=test,DC=test,DC=local"]

#------------------------ ADD USER

adduser = {
           "objectClass": ["user","person","posixAccount","shadowAccount","organizationalPerson"],
           "sn": [primer_apellido + " " + segundo_apellido],
           "uid": [nombre + " " + primer_apellido + " " + segundo_apellido], 
           "mail": [login + "@test.com"],
	 	   "givenName": [nombre],
           "displayName": [nombre + " " + primer_apellido + " " + segundo_apellido],
           "sAMAccountName":[login],
           "userPrincipalName":[login+"@test.local"],
           "userAccountControl":["544"],
           "homeDirectory": ["\\\\nas_1500\usuario\%username%"],
           "homeDrive": ["U:"]

         }


result = con.add_s(dn, ldap.modlist.addModlist(adduser))
print("Usuario creado: ", dn)

#------------------------ ADD USER TO GROUP
add_member = [(ldap.MOD_ADD, 'member', dn)]
for group_dn in group_dn:
        con.modify_s(group_dn, add_member)

